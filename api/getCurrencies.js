export default async function getCurrencies({ newDate }) {
  try {
    const response = await fetch(
      `https://freecurrencyapi.net/api/v2/historical?apikey=d20cbae0-5cbc-11ec-be53-f5c295280e78&base_currency=USD&date_from=${newDate}&date_to=${newDate}`
    );
    const result = await response.json();
    return result;
  } catch (e) {
    throw e;
  }
}
