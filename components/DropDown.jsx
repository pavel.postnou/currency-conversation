import React from "react";
import DropDownPicker from "react-native-dropdown-picker";
import {styles} from "./dropdown.style";

const DropDown = ({ items, onChangeValue }) => {
  const [open, setOpen] = React.useState(false);
  const [value, setValue] = React.useState(null);

  return (
    <DropDownPicker
      data={value}
      style={styles.dropDown}
      labelStyle={styles.dropLabel}
      textStyle={styles.dropText}
      open={open}
      onChangeValue={onChangeValue}
      value={value}
      items={items}
      setOpen={setOpen}
      setValue={setValue}
    />
  );
};

export default React.memo(DropDown);
