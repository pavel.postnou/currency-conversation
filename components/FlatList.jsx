import React, { useCallback, useState } from "react";
import { FlatList } from "react-native";
import Item from "./Item";

const Flat = ({ items }) => {
  const arr = items.slice(0, 30);
  const [offset, setOffset] = useState(31);
  const [limit, setLimit] = useState(60);
  const [res, setRes] = useState(arr);
  const renderItem = ({ item }) => <Item data={item} />;
  const keyPut = useCallback((item) => item.label, []);
  const fetchResult = () => {
    setRes(res.concat(items.slice(offset, limit)));
    setOffset(limit + 1);
    setLimit(limit + 30);
  };

  return (
    <FlatList
      data={res}
      renderItem={renderItem}
      keyExtractor={keyPut}
      onEndReached={() => fetchResult()}
      onEndReachedThreshold={0.2}
    />
  );
};

export default React.memo(Flat);
