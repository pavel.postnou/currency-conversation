import React, { useCallback } from "react";
import { TextInput } from "react-native";
import { styles } from "./input.styles"

const Input = ({ setAmount, amount }) => {
  const checkingType = useCallback(
    (value) => (!isNaN(value) ? setAmount(value) : null),
    []
  )
  return (
    <TextInput
      style={styles.textInput}
      placeholder="amount"
      onChangeText={checkingType}
      value={amount}
    />
  );
};

export default React.memo(Input);
