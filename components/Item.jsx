import React from "react";
import { View } from "react-native";
import TextComponent from "./Text";
import { styles } from "./item.style";

const Item = ({ data }) => (
  <View style={styles.item}>
    <TextComponent type="label" text={data.label} />
    <TextComponent type="value" text={data.value} />
  </View>
);

export default React.memo(Item);
