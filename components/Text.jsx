import React from "react";
import { Text } from "react-native";
import { styles } from "./text.style";

const TextComponent = ({ type, text }) => (
  <Text style={styles[type]}>{text}</Text>
);

export default React.memo(TextComponent);
