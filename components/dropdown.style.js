import { StyleSheet } from "react-native";

export const styles = StyleSheet.create({
  dropDown: {
    marginBottom: 5,
    backgroundColor: "#baddff",
    zIndex: 2,
  },
  dropLabel: {
    fontWeight: "bold",
    color: "red",
  },
  dropText: {
    fontSize: 20,
  },
});
