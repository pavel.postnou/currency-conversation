import { StyleSheet } from "react-native";

export const styles = StyleSheet.create({
  textInput: {
    width: "30%",
    textAlign: "center",
    borderWidth: 2,
    borderColor: "red",
    marginVertical: 10,
    borderRadius: 5,
    backgroundColor: "white",
  },
});
