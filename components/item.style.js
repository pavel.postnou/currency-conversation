import { StyleSheet } from "react-native";

export const styles = StyleSheet.create({
    item: {
      flexDirection: "row",
      backgroundColor: "#baddff",
      justifyContent: "center",
      padding: 5,
      marginVertical: 2,
      marginHorizontal: 16,
      borderRadius:10
    },
  });