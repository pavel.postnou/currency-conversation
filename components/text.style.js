import { StyleSheet } from "react-native";

export const styles = StyleSheet.create({
  label: {
    fontSize: 17,
    fontWeight: "bold",
    color: "#f50000",
  },
  value: {
    fontSize: 15,
    marginLeft: 10,
    fontWeight: "bold",
  },
});
