import {
  Button,
  SafeAreaView,
  ActivityIndicator,
  View,
  ImageBackground,
} from "react-native";
import React, { useState, useEffect, useCallback } from "react";
import Communications from "react-native-communications";
import Dialog from "react-native-dialog";
import AsyncStorage from "@react-native-async-storage/async-storage";
import DateTimePicker from "@react-native-community/datetimepicker";
import TextComponent from "../../components/Text";
import { styles } from "./currencyScreen.style";
import getCurrencies from "../../api/getCurrencies";
import uuid from "react-native-uuid";
import DropDown from "../../components/DropDown";
import Flat from "../../components/FlatList";
import Input from "../../components/Input";

export default function currencyScreen() {
  const [loading, setLoading] = useState(false);
  const [items, setItems] = React.useState([]);
  const [firstValue, setFirstValue] = React.useState(null);
  const [secondValue, setSecondValue] = React.useState(null);
  const [amount, setAmount] = React.useState();
  const [fLabel, setFLabel] = React.useState();
  const [sLabel, setSLabel] = React.useState();
  const [visible, setVisible] = React.useState(false);
  const [emailTo, setEmailTo] = React.useState("");
  const [correct, setCorrect] = React.useState(false);
  const [correctColor, setCorrectColor] = React.useState("red");
  const [sendColor, setSendColor] = React.useState("grey");
  const [disable, setDisable] = React.useState(true);
  const [exchange, setExchange] = React.useState(null);
  const [archive, setArchive] = React.useState([]);
  const [date, setDate] = useState(new Date(Date.now()));
  const [mode, setMode] = useState("date");
  const [show, setShow] = useState(false);

  const onChange = (event, selectedDate) => {
    const currentDate = selectedDate || date;
    setShow(false);
    currentDate && currentDate != date
      ? (setDate(currentDate), getCurr(currentDate))
      : null;
  };

  const showMode = (currentMode) => {
    setShow(true);
    setMode(currentMode);
  };

  const showDatepicker = () => {
    showMode("date");
  };

  const getCurr = useCallback(async (date) => {
    try {
      let currencies = [];
      const newDate = await date.toISOString().substring(0, 10);
      setLoading(true);
      const result = await getCurrencies({ newDate });
      let ordered = {};
      Object.keys(result.data[newDate])
        .sort()
        .forEach(function (key) {
          ordered[key] = result.data[newDate][key];
        });
      for (let prop in ordered)
        currencies.push({ label: prop, value: ordered[prop] });
      setItems(currencies);
    } catch (e) {
      alert(e);
    } finally {
      setLoading(false);
    }
  });

  const converse = useCallback(
    (val, key) => {
      const getLabel = () => {
        for (let i = 0; i < items.length; i++) {
          let b = items.findIndex((item) => item.value == val);
          return b == -1 ? "" : items[b].label;
        }
      };
      const lab = getLabel();
      if (key == 1) {
        setFLabel(lab), setFirstValue(val);
      } else if (key == 2) {
        setSLabel(lab), setSecondValue(val);
      }
    },
    [items]
  );

  useEffect(() => {
    amount && fLabel && sLabel
      ? setExchange(
          amount +
            " " +
            fLabel +
            " = " +
            ((amount * secondValue) / firstValue).toFixed(2) +
            " " +
            sLabel
        )
      : null;
  }, [amount, firstValue, secondValue]);

  useEffect(() => {
    const check =
      /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    check.test(emailTo)
      ? (setCorrect("your email is valid"),
        setCorrectColor("blue"),
        setSendColor("#24baff"),
        setDisable(false))
      : (setCorrect("invalid email"),
        setCorrectColor("red"),
        setSendColor("rgba(189, 189, 189, 0.5)"),
        setDisable(true));
  }, [emailTo]);

  useEffect(async () => {
    let temporary = [];
    if (exchange) {
      try {
        temporary = archive;
        if (temporary.length > 3) {
          temporary.pop();
          temporary.unshift(exchange);
          setArchive(temporary);
          const jsonValue = JSON.stringify(temporary);
          await AsyncStorage.setItem("arch", jsonValue);
        } else {
          temporary.unshift(exchange);
          setArchive(temporary);
          const jsonValue = JSON.stringify(temporary);
          await AsyncStorage.setItem("arch", jsonValue);
        }
      } catch (e) {
        alert(e);
      }
    }
  }, [exchange]);

  useEffect(async () => {
    try {
      const jsonValue = await AsyncStorage.getItem("arch");
      jsonValue != null ? setArchive(JSON.parse(jsonValue)) : null;
    } catch (e) {
      alert(e);
    }
  }, []);

  const handleCancel = () => {
    setVisible(false);
  };
  const handleSend = () => {
    setVisible(false);
    Communications.email([emailTo], null, null, "conversation", exchange);
  };
  const alertSend = () => {
    setVisible(true);
  };

  return (
    <View>
      <ImageBackground
        source={require("../../assets/back.jpg")}
        resizeMode="cover"
        style={styles().image}
      >
        <View style={styles().allContainer}>
          {!loading ? (
            <SafeAreaView style={styles().container}>
              <TextComponent type="label" text="Exchange rates" />
              <Flat items={items} />
            </SafeAreaView>
          ) : (
            <View style={styles().container}>
              <ActivityIndicator size="large" color="blue" />
            </View>
          )}
          <View style={styles().containerCur}>
            <TextComponent type="label" text="Set amount" />
            <Input setAmount={setAmount} amount={amount} />
            <TextComponent type="label" text="Choose first currency" />
            <DropDown
              items={items}
              onChangeValue={(value) => converse(value, 1)}
            />
            <DropDown
              items={items}
              onChangeValue={(value) => converse(value, 2)}
            />
            <TextComponent type="label" text="Choose second currency" />
            <TextComponent type="value" text={exchange} />
            <TextComponent type="label" text="Last results" />
            <View style={styles().historyView}>
              {archive && archive.length > 0
                ? archive.map((item) => (
                    <TextComponent key={uuid.v4()} type="value" text={item} />
                  ))
                : null}
            </View>
            <View style={styles().buttonContainer}>
              <Button
                title={"today\ncurrencies"}
                onPress={() => (
                  setDate(new Date(Date.now())), getCurr(new Date(Date.now()))
                )}
              ></Button>
              <View style={styles().buttonDate}>
                <Button
                  title={"currencies\nby date"}
                  onPress={showDatepicker}
                ></Button>
              </View>
            </View>
            <Button title="share result" onPress={() => alertSend()}></Button>
            <Dialog.Container visible={visible} onBackdropPress={handleCancel}>
              <Dialog.Title style={styles().dialogTitle}>
                Share card
              </Dialog.Title>
              <Dialog.Description style={styles().dialogDescription}>
                Write email address below
              </Dialog.Description>
              <Dialog.Input
                style={styles().dialogInput}
                placeholder="example@mail.com"
                onChangeText={setEmailTo}
                value={emailTo}
              />
              <Dialog.Description style={styles(correctColor).mailDialog}>
                {correct}
              </Dialog.Description>
              <Dialog.Button
                label="Send"
                color={sendColor}
                onPress={handleSend}
                disabled={disable}
              />
              <Dialog.Button
                label="Cancel"
                onPress={handleCancel}
                color="red"
              />
            </Dialog.Container>
            {show && (
              <DateTimePicker
                testID="dateTimePicker"
                value={date}
                mode={mode}
                is24Hour={true}
                display="default"
                onChange={onChange}
              />
            )}
          </View>
        </View>
      </ImageBackground>
    </View>
  );
}
