import { StyleSheet } from "react-native";

export const styles = (data) =>
  StyleSheet.create({
    container: {
      flex: 1,
      alignItems: "center",
    },
    containerCur: {
      flex: 1,
      justifyContent: "space-between",
      alignItems: "center",
      marginRight: 10,
    },
    allContainer: {
      flex: 1,
      flexDirection: "row",
      marginTop: 30,
    },
    textInput: {
      width: "30%",
      textAlign: "center",
      borderWidth: 2,
      borderColor: "red",
      marginVertical: 10,
      borderRadius: 5,
      backgroundColor: "white",
    },
    dropDown: {
      marginBottom: 5,
      backgroundColor: "#baddff",
      zIndex: 2,
    },
    dropDownSecond: {
      marginBottom: 5,
      backgroundColor: "#baddff",
      zIndex: 1,
    },
    buttonContainer: {
      marginBottom: 5,
      flexDirection: "row",
    },
    buttonDate: {
      marginLeft: 5,
    },
    image: {
      width: "100%",
      height: "100%",
    },
    dropLabel: {
      fontWeight: "bold",
      color: "red",
    },
    dropText: {
      fontSize: 20,
    },
    historyView: {
      backgroundColor: "white",
      borderRadius: 10,
      padding: 10,
    },
    dialogTitle: {
      color: "#24baff",
    },
    dialogDescription: {
      color: "#8f8731",
    },
    dialogInput: {
      color: "black",
    },
    mailDialog: {
      color: data,
      fontSize: 13,
    },
  });
